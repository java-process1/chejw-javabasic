package com.chejw.cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChejwCode01Application {

    public static void main(String[] args) {
        SpringApplication.run(ChejwCode01Application.class, args);
    }

}
